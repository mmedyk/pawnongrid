﻿using System;

namespace PawnOnGrid
{
    public class Grid
    {
        public int Length { get; private set; }
        public int Depth { get; private set; }

        private Pawn pawn;

        public Grid(int length, int depth)
        {
            Length = length;
            Depth = depth;
        }

        public Pawn SetPawn(int positionX, int positionY, Direction frontside)
        {
            pawn = new Pawn(this, positionX, positionY, frontside);
            return pawn;
        }

        public Pawn GetPawn()
        {
            return pawn;
        }

        public void MovePawn(string moveSequence)
        {
            foreach (char cmd in moveSequence)
            {
                switch (cmd)
                {
                    case 'L':
                        pawn.Rotate(Side.Left);
                        break;
                    case 'R':
                        pawn.Rotate(Side.Right);
                        break;
                    case 'M':
                        pawn.Move();
                        break;
                    default:
                        break;
                }
                Console.WriteLine($"{cmd} {pawn}");
            }
        }

        public bool IsPositionValid(Coordinates coord)
        {
            // NOTE: there is one quirk, coordinates on the grid start at 0 and end at dimension value
            // so its actual length and depth are +1 larger

            return coord.X >= 0 && coord.X <= Length
                && coord.Y >= 0 && coord.Y <= Depth;
        }

        public override string ToString()
        {
            return $"{Length} {Depth}";
        }
    }
}
