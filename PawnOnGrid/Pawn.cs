﻿namespace PawnOnGrid
{
    public class Pawn
    {
        public Coordinates Position { get; private set; }
        public Direction Frontside { get; private set; }
        public Grid Controller { get; private set; }

        internal Pawn(Grid controller, int posX, int posY, Direction frontside)
        {
            Controller = controller;
            Position = new Coordinates() { X = posX, Y = posY };
            Frontside = frontside;
        }

        public void Move()
        {
            Coordinates newPos = new Coordinates(Position);

            switch (Frontside)
            {
                case Direction.North:
                    newPos.Y++;
                    break;
                case Direction.East:
                    newPos.X++;
                    break;
                case Direction.South:
                    newPos.Y--;
                    break;
                case Direction.West:
                    newPos.X--;
                    break;
                default:
                    break;
            }

            if (Controller.IsPositionValid(newPos))
            {
                Position = newPos;
            }
            else
            {
                // ignore, do not move to new position
            }
        }

        public void Rotate(Side rotationSide)
        {
            int result = (int)Frontside + (int)rotationSide;

            bool normalized = false;
            while (!normalized)
            {
                if (result < 0) result += 360;
                else if (result >= 360) result -= 360;
                else normalized = true;
            }

            Frontside = (Direction)result;
        }

        public override string ToString()
        {
            return $"{Position.X} {Position.Y} {Frontside}";
        }
    }
}