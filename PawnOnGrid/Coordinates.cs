﻿namespace PawnOnGrid
{
    public struct Coordinates
    {
        public int X, Y;

        public Coordinates(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public Coordinates(Coordinates coord) : this()
        {
            X = coord.X;
            Y = coord.Y;
        }

        public void Set(Coordinates coord)
        {
            X = coord.X;
            Y = coord.Y;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Coordinates)) return false;

            Coordinates coord = (Coordinates)obj;
            return coord.X == X && coord.Y == Y;
        }

        public override int GetHashCode()
        {
            return unchecked(X ^ Y);
        }

        public override string ToString()
        {
            return $"{X} {Y}";
        }
    }
}