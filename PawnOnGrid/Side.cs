﻿namespace PawnOnGrid
{
    /// <summary>
    /// Rotation side
    /// </summary>
    public enum Side : int
    {
        Left = -90,
        Right = 90
    }
}