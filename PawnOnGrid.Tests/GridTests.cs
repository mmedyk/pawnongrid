﻿using NUnit.Framework;

namespace PawnOnGrid.Tests
{
    [TestFixture]
    public class GridTests
    {
        [Test]
        public void MovePawn_ExampleCase1()
        {
            var grid = new Grid(5, 5);
            var pawn = grid.SetPawn(1, 2, Direction.North);

            grid.MovePawn("LMLMLMLMM");

            Assert.That(pawn.Position, Is.EqualTo(new Coordinates(1, 3)));
            Assert.That(pawn.Frontside, Is.EqualTo(Direction.North));
        }

        [Test]
        public void MovePawn_ExampleCase2()
        {
            var grid = new Grid(5, 5);
            var pawn = grid.SetPawn(3, 3, Direction.East);

            grid.MovePawn("MMRMMRMRRM");

            Assert.That(pawn.Position, Is.EqualTo(new Coordinates(5, 1)));
            Assert.That(pawn.Frontside, Is.EqualTo(Direction.East));
        }

        [Test]
        public void IsPositionValid_InsideGrid_IsValid()
        {
            var grid = new Grid(5, 5);
            var coord = new Coordinates(2, 3);

            bool result = grid.IsPositionValid(coord);

            Assert.That(result, Is.True);
        }

        [Test]
        public void IsPositionValid_OnGridBorder_IsValid()
        {
            var grid = new Grid(5, 5);
            var coord = new Coordinates(0, 5);

            bool result = grid.IsPositionValid(coord);

            Assert.That(result, Is.True);
        }

        [Test]
        public void IsPositionValid_OutsideGrid_NotValid()
        {
            var grid = new Grid(5, 5);
            var coord = new Coordinates(3, 6);

            bool result = grid.IsPositionValid(coord);

            Assert.That(result, Is.False);
        }
    }
}
