﻿using NUnit.Framework;

namespace PawnOnGrid.Tests
{
    [TestFixture]
    public class PawnTests
    {
        [Test]
        public void Move_NorthDirection_IncreasedY()
        {
            var grid = new Grid(5, 5);
            var pawn = grid.SetPawn(2, 2, Direction.North);

            pawn.Move();

            Assert.That(pawn.Position, Is.EqualTo(new Coordinates(2, 2 + 1)));
            Assert.That(pawn.Frontside, Is.EqualTo(Direction.North));
        }

        [TestCase(Direction.North, 0, 1)]
        [TestCase(Direction.East, 1, 0)]
        [TestCase(Direction.South, 0, -1)]
        [TestCase(Direction.West, -1, 0)]
        public void Move_ToPositionOnGrid_CoordinatesUpdated(Direction frontside, int dx, int dy)
        {
            int posX = 2;
            int posY = 2;
            var grid = new Grid(5, 5);
            var pawn = grid.SetPawn(posX, posY, frontside);

            pawn.Move();

            Assert.That(pawn.Position, Is.EqualTo(new Coordinates(posX + dx, posY + dy)));
            Assert.That(pawn.Frontside, Is.EqualTo(frontside));
        }

        [TestCase(4, 2, 4, Direction.North)]
        [TestCase(4, 2, 0, Direction.South)]
        [TestCase(4, 4, 2, Direction.East)]
        [TestCase(4, 0, 2, Direction.West)]
        public void Move_FromGridBorderOutsideGrid_PositionNotChanged(int gridSize, int posX, int posY, Direction frontside)
        {
            var grid = new Grid(gridSize, gridSize);
            var pawn = grid.SetPawn(posX, posY, frontside);

            pawn.Move();

            Assert.That(pawn.Position, Is.EqualTo(new Coordinates(posX, posY)));
            Assert.That(pawn.Frontside, Is.EqualTo(frontside));
        }

        [Test]
        public void Rotate_NorthToRight_IsWest()
        {
            var grid = new Grid(5, 5);
            var pawn = grid.SetPawn(2, 2, Direction.North);

            pawn.Rotate(Side.Right);

            Assert.That(pawn.Position, Is.EqualTo(new Coordinates(2, 2)));
            Assert.That(pawn.Frontside, Is.EqualTo(Direction.East));
        }

        [TestCase(Direction.North, Direction.East)]
        [TestCase(Direction.East, Direction.South)]
        [TestCase(Direction.South, Direction.West)]
        [TestCase(Direction.West, Direction.North)]
        public void Rotate_ToRight(Direction initialFrontside, Direction resultFrontside)
        {
            var grid = new Grid(5, 5);
            var pawn = grid.SetPawn(2, 2, initialFrontside);

            pawn.Rotate(Side.Right);

            Assert.That(pawn.Position, Is.EqualTo(new Coordinates(2, 2)));
            Assert.That(pawn.Frontside, Is.EqualTo(resultFrontside));
        }

        [TestCase(Direction.North, Direction.West)]
        [TestCase(Direction.West, Direction.South)]
        [TestCase(Direction.South, Direction.East)]
        [TestCase(Direction.East, Direction.North)]
        public void Rotate_ToLeft(Direction initialFrontside, Direction resultFrontside)
        {
            var grid = new Grid(5, 5);
            var pawn = grid.SetPawn(2, 2, initialFrontside);

            pawn.Rotate(Side.Left);

            Assert.That(pawn.Position, Is.EqualTo(new Coordinates(2, 2)));
            Assert.That(pawn.Frontside, Is.EqualTo(resultFrontside));
        }
    }
}
